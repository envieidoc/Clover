/* Generated automatically. */
static const char configuration_arguments[] = "/Users/envieidoc/CloverGrowerPro/src/gcc/gcc-4.9.3/configure --host=x86_64-apple-darwin15.4.0 --build=x86_64-apple-darwin15.4.0 --target=x86_64-clover-linux-gnu --prefix=/Users/envieidoc/CloverGrowerPro/toolchain/cross --with-sysroot=/Users/envieidoc/CloverGrowerPro/toolchain --with-gmp=/Users/envieidoc/CloverGrowerPro/toolchain --with-mpfr=/Users/envieidoc/CloverGrowerPro/toolchain --with-mpc=/Users/envieidoc/CloverGrowerPro/toolchain --with-isl=/Users/envieidoc/CloverGrowerPro/toolchain --with-cloog=/Users/envieidoc/CloverGrowerPro/toolchain --with-system-zlib --with-gnu-as --with-gnu-ld --with-newlib --disable-libssp --disable-nls --disable-werror --enable-languages=c,c++ --enable-cloog-backend=isl --enable-plugin --disable-isl-version-check";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "x86-64" } };
