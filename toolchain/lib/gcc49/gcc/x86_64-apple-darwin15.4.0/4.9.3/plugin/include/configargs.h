/* Generated automatically. */
static const char configuration_arguments[] = "/Users/envieidoc/CloverGrowerPro/src/gcc/gcc-4.9.3/configure --prefix=/Users/envieidoc/CloverGrowerPro/toolchain --with-sysroot=/Users/envieidoc/CloverGrowerPro/toolchain/sdk --enable-languages=c,c++,objc --libdir=/Users/envieidoc/CloverGrowerPro/toolchain/lib/gcc49 --includedir=/Users/envieidoc/CloverGrowerPro/toolchain/include/gcc49 --datarootdir=/Users/envieidoc/CloverGrowerPro/toolchain/share/gcc49 --with-gettext=/Users/envieidoc/CloverGrowerPro/toolchain --with-system-zlib --disable-nls --enable-plugin --with-gxx-include-dir=/Users/envieidoc/CloverGrowerPro/toolchain/include/gcc49/c++/ --with-gmp=/Users/envieidoc/CloverGrowerPro/toolchain --with-mpfr=/Users/envieidoc/CloverGrowerPro/toolchain --with-mpc=/Users/envieidoc/CloverGrowerPro/toolchain --with-isl=/Users/envieidoc/CloverGrowerPro/toolchain --with-cloog=/Users/envieidoc/CloverGrowerPro/toolchain --enable-cloog-backend=isl --disable-bootstrap --disable-isl-version-check";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "core2" } };
